//
//  MovieDetailViewController.swift
//  MovieApp
//
//  Created by Davoodi, Alireza on 2018-10-30.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

protocol MovieDetailViewControllerProtocol : AnyObject{
    func movieDidGetUpdated(movieDtailViewController: MovieDetailViewController, movie: Movie)
}

class MovieDetailViewController: UIViewController {

    weak var delegate: MovieDetailViewControllerProtocol? = nil

    var clicked: Int = 0

    var movie: Movie?  = nil

    @IBOutlet weak var movieImageView: UIImageView!

    @IBOutlet weak var titleTextField: UITextField!

    @IBOutlet weak var descriptionTextField: UITextField!

    @IBOutlet weak var editBarButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {

        if let img = self.movie?.image
        {
            self.movieImageView.image = UIImage(named: img)
        }

        if let mtitle = self.movie?.title
        {
            self.titleTextField.text = mtitle
        }

        if let mDesc = self.movie?.desc
        {
            self.descriptionTextField.text = mDesc
        }

        self.editBarButton.title = "edit"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func editAction(_ sender: UIBarButtonItem) {

        clicked = clicked+1

        if clicked%2==1
        {
            sender.title = "done"
            self.titleTextField.isEnabled = true
            self.descriptionTextField.isEnabled = true

            self.titleTextField.becomeFirstResponder()
        }
        else {
            sender.title = "edit"

            if self.titleTextField.isFirstResponder
            {
                self.titleTextField.resignFirstResponder()
            }
            else if self.descriptionTextField.isFirstResponder
            {
                self.descriptionTextField.resignFirstResponder()
            }

            self.titleTextField.isEnabled = false
            self.descriptionTextField.isEnabled = false
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        let newTitle = self.titleTextField.text
        let newDesc = self.descriptionTextField.text

        let newMovie: Movie = Movie(image: self.movie?.image, title: newTitle, desc: newDesc)

        self.delegate?.movieDidGetUpdated(movieDtailViewController: self, movie: newMovie)
    }
}
