//
//  MovieSectionHeaderView.swift
//  MovieApp
//
//  Created by admin on 2018-10-25.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class MovieSectionHeaderView: UIView {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MovieHeaderXib", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
